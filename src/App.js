import React from 'react';
import { ImgList } from './features/imgList/ImgList';
import { Banner } from './BaseComponents/Banner/Banner';
import { Content } from './BaseComponents/Content/Content';
import './App.css';

function App() {
  return (
    <div className="App">
      <Banner>
        Technical Exercise
      </Banner>
      <Content>
        <ImgList />
      </Content>
      <Banner>
        Built By: Rodrigo Paris
      </Banner>
    </div>
  );
}

export default App;
