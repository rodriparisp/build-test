import styled from "styled-components";

export const Content = styled.div`
  height:88vh;
  overflow: auto;
`;