import styled from "styled-components";

export const Banner = styled.div`
  width: 100vw;
  height: 6vh;
  background-color: gray;
  color: white;
  font-size: 18px;
  font-weight: bold;
  line-height: 50px;
`;