import { configureStore } from '@reduxjs/toolkit';
import counterReducer from '../features/counter/counterSlice';
import imgListReducer from '../features/imgList/imgListSlice';

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    images: imgListReducer
  },
});
