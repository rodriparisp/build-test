import styled from 'styled-components';

export const Base = styled.div`
  display: flex;
`;

export const ImagesContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 50vw;
  background-color: orange;
  align-items: center;
  row-gap: 50px;
  overflow: auto;
  height: 100%;
`;

export const ImageHighlight = styled.div`
  width: 50vw;
  background-color: yellow;
  display: flex;
  position: fixed;
  right: 0;
  height: 88vh;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  row-gap: 30px;
`;