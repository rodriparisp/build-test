import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { fetchImages } from './imgListAPI';

const initialState = {
  list: [],
};

export const getImages = createAsyncThunk(
  'imgList/fetchImages',
  async () => {
    const response = await fetchImages();
    return response;
  }
);

export const imgListSlice = createSlice({
  name: 'imgList',
  initialState,
  reducers: {
    //TODO: Add images from drive.
  },
  extraReducers: (builder) => {
    builder
      .addCase(getImages.fulfilled, (state, action) => {
        state.list = action.payload;
      });
  },
});

//export const {  } = imgListSlice.actions;

export const selectList = (state) => state.images.list;

export default imgListSlice.reducer;
