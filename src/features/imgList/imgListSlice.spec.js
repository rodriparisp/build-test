import imgListReducer, {
  //getImages
} from './imgListSlice';

describe('imgList reducer', () => {
  // const initialState = {
  //   list: []
  // };
  it('should handle initial state', () => {
    expect(imgListReducer(undefined, { type: 'unknown' })).toEqual({
      list: []
    });
  });
});
