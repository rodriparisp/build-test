import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  getImages,
} from './imgListSlice';
import {
  ImagesContainer,
  ImageHighlight,
  Base
} from './styles';

export function ImgList() {
  const { list } = useSelector((state) => state.images);
  const [ selectedImage, setSelectedImage ] = useState(null);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getImages());
  }, [dispatch]);

  const selectImage = (img) => {
    setSelectedImage(img);
  };

  const clearSelection = () => {
    setSelectedImage(null);
  }

  return (
    <Base>
      <ImagesContainer>
        {list.length > 0 && list.map((pImg) => (
          <img src={pImg.download_url} key={pImg.id} alt="Random" height="200" width="200" onClick={()=>selectImage(pImg)}/>
        ))}
      </ImagesContainer>
      <ImageHighlight>
          {selectedImage &&
            <>
              <h2>{selectedImage.author}</h2>
              <img src={selectedImage.download_url} alt="Selected" height="200" width="200"/>
              <div>
                <button onClick={clearSelection}>Clear</button>
              </div>
            </>
          }
      </ImageHighlight>
    </Base>
  );
}
