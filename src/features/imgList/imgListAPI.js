export function fetchImages() {
  return new Promise((resolve) =>
  fetch('https://picsum.photos/v2/list')
    .then(response => response.json())
    .then(data => resolve(data))
  );
}
